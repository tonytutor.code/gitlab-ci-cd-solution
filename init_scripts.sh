#!/bin/bash


# Creat scripts/gitlab/install.sh package 
PATH="scripts/gitlab/"
FNAME="install.sh"

CODEHEAD="\
"
FCODE="\
"

# Create  scripts/runner/install.sh


# Create output/stderr/print_err.print_err.sprint_err.sh
INTCODE="""
err() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*" >&2
}

if ! do_something; then
  err "Unable to do_something"
  exit 1
fi
"""
echo "$INTCODE"
